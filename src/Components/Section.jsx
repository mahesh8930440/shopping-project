import { Link } from "react-router-dom"
const Section=()=>{
    return (
        <div className="mx-[10%] py-8 text-center">
            <Link to="/list/mens_outerwear">
                <div className="my-6">
                    <img src="https://shop.polymer-project.org/esm-bundled/images/mens_outerwear.jpg"/>
                    <h2 className="py-6">Men's Outerwear</h2>
                    <button className="border border-slate-700 px-4 py-2">SHOW NOW</button>
                </div>
            </Link>
            <Link to="/list/ladies_outerwear">
                <div className="my-6">
                    <img src="https://shop.polymer-project.org/esm-bundled/images/ladies_outerwear.jpg"/>
                    <h2 className="py-6">Ladies Outerwear</h2>
                    <button className="border border-slate-700 px-4 py-2">SHOW NOW</button>
                </div>
            </Link>    
            <div className="flex my-6">
                <Link to="/list/mens_tshirts">
                    <div>
                        <img src="https://shop.polymer-project.org/esm-bundled/images/mens_tshirts.jpg" />
                        <h2 className="py-6">Men's T-shirts</h2>
                        <button className="border border-slate-700 px-4 py-2">SHOW NOW</button>
                    </div>
                </Link>
                <Link to="list/ladies_tshirts">
                    <div>
                        <img src="https://shop.polymer-project.org/esm-bundled/images/ladies_tshirts.jpg"/>
                        <h2 className="py-6">Ladies T-shirts</h2>
                        <button className="border border-slate-700 px-4 py-2">SHOW NOW</button>
                    </div>
                </Link>    
            </div>    
        </div>
    )
}
export default Section