import {useState,useEffect} from 'react';
import ProductDetails from './ProductDetails';
import { listOfMensOutwearItems } from '../Components/data';
const MensOuterwear=()=>{
    const [listOfProductItems,setListOfProductItems]=useState([]);
    useEffect(()=>{
        fetchingOuterwear();
    }, [])
    
    function fetchingOuterwear(){
        let  outerwearData=listOfMensOutwearItems;
        setListOfProductItems(outerwearData);
    }

    return (
        <div className="mx-[10%] py-8 text-center">
            <div className="my-6">
               <img src="https://shop.polymer-project.org/esm-bundled/images/mens_outerwear.jpg"/>
               <h2 className="py-6">Men's Outerwear</h2>
               <p className="px-4">(16 items)</p>
            </div>
            <ProductDetails listOfProductItems={listOfProductItems}/>
        </div>
    )
}
export default MensOuterwear