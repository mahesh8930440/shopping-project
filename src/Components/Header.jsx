import {Link,useLocation} from "react-router-dom";
import { useContext } from "react";
import { CartContext } from "../Components/Context";
const Header=()=>{
    const location = useLocation();
    const isCartComponent = location.pathname === '/cart';
    const cartItems = useContext(CartContext);
    const cartItemsCount=cartItems.cart.length;
    return (
        
        <div className="px-[2%]">
            <div className="flex py-8 justify-between pl-[47%] text-center">
                <Link to="/" ><h1 className="text-[1.5rem] ">SHOP</h1></Link>
                <div>
                    <Link to="/cart"><i class="fa-solid fa-cart-shopping "></i></Link>
                    <sup>{cartItemsCount}</sup>
                </div>    
            </div>
            {!isCartComponent && (<nav className="list-none flex justify-center gap-[2%] ">
                <Link to="/list/mens_outerwear"><li>Men's Outerwear</li></Link>
                <Link to="/list/ladies_outerwear"><li>Ladies Outerwear</li></Link>
                <Link to="/list/mens_tshirts"><li>Men's T-shirts</li></Link>
                <Link to="list/ladies_tshirts"><li>Ladies T-shirts</li></Link>
            </nav>)} 
        </div>
    )
}
export default Header;
