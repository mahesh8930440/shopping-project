import {useState,useEffect} from 'react';
import ProductDetails from './ProductDetails';
import { listOfLadiesOuterwearItems } from '../Components/data';
const LadiesOuterwear=()=>{
    const [listOfProductItems,setListOfProductItems]=useState([]);
    useEffect(()=>{
        fetchingOuterwear();
    }, [])
    
    function fetchingOuterwear(){
        let response=listOfLadiesOuterwearItems;
        setListOfProductItems(response);
    }

    return (
        <div className="mx-[10%] py-8 text-center">
            <div className="my-6">
                <img src="https://shop.polymer-project.org/esm-bundled/images/ladies_outerwear.jpg"/>
                <h2 className="py-6">Ladies Outerwear</h2>
                <p className="px-4">(6 items)</p>
            </div>
            <ProductDetails listOfProductItems={listOfProductItems}/>
        </div>
    )
}
export default LadiesOuterwear