import { createContext, useState } from "react";

export const CartContext = createContext();

export const CartProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  const [isHidden, setIsHidden] = useState(false);
  return (
    <CartContext.Provider value={{ cart, setCart,isHidden, setIsHidden }}>
      {children}
    </CartContext.Provider>
  );
};
