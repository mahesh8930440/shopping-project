import { useParams } from "react-router-dom";
import { useContext, useState, useEffect } from "react";
import { CartContext } from "../Components/Context";

const Cart = () => {
  const cartItems = useContext(CartContext);
  const [totalPrice,setTotalprice]=useState(0);
  const handleTotalPrice=()=>{

    let price=cartItems.cart.reduce((acc,cartDetail)=>{
        acc +=parseInt(cartDetail.quantity)*cartDetail.price;
        return acc
    },0)
    setTotalprice(price.toFixed(2))
  }
  useEffect(()=>{
    handleTotalPrice();
  },[cartItems.cart])
  const handleQuantityChange = (event, cartDetail) => {
    cartDetail.quantity = parseInt(event.target.value);
    cartItems.setCart([...cartItems.cart])
  };

  const handleRemoveItem = (index) => {
    cartItems.cart.splice(index, 1);
    cartItems.setCart([...cartItems.cart])
  };
  
  return (
    <div className="w-[70%] mx-auto mt-[5%]">
      <div className="text-center text-[1.2rem]">
            <h2 className="mb-2">Your Cart</h2>
            <p className=" text-gray-600">({cartItems.cart.length}  Item)</p>
      </div>
      {cartItems.cart.map((cartDetail, index) => (
        <div key={index} className="flex justify-between my-8 text-gray-600">
         
          <div className="flex mr-4 w-[30%]">
            <img src={`https://shop.polymer-project.org/esm-bundled/${cartDetail.image}`} alt={cartDetail.name} className="h-[40px] w-[40px]"/>
            <h2 className=" align-middle">{cartDetail.title}</h2>
          </div>  
          <div>
            <label htmlFor="quantity">Quantity:</label>
            <select
                id="quantity"
                value={cartDetail.quantity}
                onChange={(event) => handleQuantityChange(event, cartDetail)}>
                {Array.from({ length: 10 }, (_, index) => (
                    <option key={index + 1}>{index + 1}</option>
                ))}
            </select>
          </div>  
          <p>Size : M</p>
          <p>${cartDetail.price}</p>
          <i
            className="fa-solid fa-trash"
            onClick={() => handleRemoveItem(index)}
          ></i>
        </div>
      ))}
      {totalPrice > 0 && <p className="float-right">Total :  ${totalPrice}</p>}
    </div>
  );
};

export default Cart;
