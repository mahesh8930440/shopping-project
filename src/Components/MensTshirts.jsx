import {useState,useEffect} from 'react';
import ProductDetails from './ProductDetails';
import { listOfMensTshirts } from '../Components/data';
const MensTshirts=()=>{
    const [listOfProductItems,setListOfProductItems]=useState([]);
    useEffect(()=>{
        fetchingOuterwear();
    }, [])
    
    async function fetchingOuterwear(){
      
        let  outerwearData=listOfMensTshirts;
        setListOfProductItems(outerwearData);
    }

    return (
        <div className="mx-[10%] py-8 text-center">
            <div className="my-6">
                <img src="https://shop.polymer-project.org/esm-bundled/images/mens_tshirts.jpg" />
                <h2 className="py-6">Men's T-shirts</h2>
               <p className="px-4">(16 items)</p>
            </div>
            <ProductDetails listOfProductItems={listOfProductItems}/>
        </div>
    )
}
export default MensTshirts