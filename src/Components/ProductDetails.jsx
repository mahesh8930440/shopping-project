import { Link } from "react-router-dom";
import { useContext } from "react";
import { CartContext } from "./Context";

const ProductDetails = ({ listOfProductItems }) => {
    const cartData = useContext(CartContext);
    function addingItemToCart(outerwearData) {
        const itemExists = cartData.cart.some(item => item.name === outerwearData.name);
        if (itemExists) {
            outerwearData["quantity"] +=1;
            cartData.setCart([...cartData.cart]);
        }
        else{
            outerwearData.quantity=1;
            cartData.setCart([...cartData.cart, outerwearData]);
        }
    }
    console.log(cartData);
    return (
        <div className="flex flex-wrap justify-around mx-[2%]">
            {listOfProductItems.length > 0 ? (
            listOfProductItems.map((outerwearData)=>{
                const {name,title,image, price}=outerwearData;
                return (<div key={name} className="basis-1/3 my-8 text-center">
                    <img src={`https://shop.polymer-project.org/esm-bundled/${image}`} alt="image" className="ml-[6%]"/>
                    <h3 className="mt-4 ">{title}</h3>
                    <div className="flex justify-center gap-1  align-middle mt-4 ">
                        <p className="mr-1">${price}</p>
                        <button className="rounded p-1 shadow bg-gray-500 text-[0.8rem]"  onClick={()=> addingItemToCart(outerwearData)}>ADD TO CART</button>
                    </div>    
                </div>)    
            })
            
            ):(<></>)}
        </div>
    
    );
};
export default ProductDetails;

