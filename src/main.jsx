import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import appRouter from './App.jsx'
import { RouterProvider } from 'react-router-dom'
import { CartProvider } from './Components/Context.jsx'
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <CartProvider>
      <RouterProvider router={appRouter}/>
    </CartProvider>
    
  </React.StrictMode>,
)
