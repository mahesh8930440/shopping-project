import Header from "./Components/Header";
import Section from "./Components/Section"
import MensOuterwear from "./Components/MensOuterwear";
import LadiesOuterwear from "./Components/LadiesOuterwear";
import LadiesTshirts from "./Components/LadiesTshirts";
import MensTshirts from "./Components/MensTshirts"; 
import Cart from "./Components/Cart"
import { createBrowserRouter,Outlet } from "react-router-dom";
function App() {
  return (
    <>
     <Header/>
     <Outlet/> 
    </>
  )
}
const appRouter=createBrowserRouter([{
  path:"/",
  element:<App/>,
  children:[
    {
      path:"/",
      element:<Section/>
    },
    {
      path:"/list/mens_outerwear",
      element:<MensOuterwear />
    },
    {
      path:"/list/ladies_outerwear",
      element:<LadiesOuterwear />
    },
    {
      path:"/list/mens_tshirts",
      element:<MensTshirts />
    },
    {
      path:"list/ladies_tshirts",
      element:<LadiesTshirts />
    },
    {
      path:"/cart",
      element:<Cart />
    }
    
  ]
}
])    

export default appRouter;




