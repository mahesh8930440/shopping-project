# Shopping App

## Getting Started

### Installation

1. Clone the repository:

- git clone git@gitlab.com:mahesh8930440/shopping-project.git

2. Navigate to the project directory:

- cd shopping-app

3. Install dependencies:

- npm install

4. Run the project:

- npm run dev
